Source: 3dldf-doc
Section: non-free/doc
Priority: optional
Maintainer: Debian TeX Task Force <debian-tex-maint@lists.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>, Norbert Preining <preining@debian.org>,
  Hilmar Preuße <hille42@debian.org>
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep:
 texinfo, texlive-base, texlive-latex-base, texlive-latex-recommended,
 texlive-plain-generic, texlive-latex-extra
Standards-Version: 4.5.0
Homepage: https://www.gnu.org/software/3dldf/
Vcs-Git: https://salsa.debian.org/tex-team/3dldf-doc.git
Vcs-Browser: https://salsa.debian.org/tex-team/3dldf-doc

Package: 3dldf-doc
Architecture: all
Depends: ${misc:Depends}
Suggests: 3dlf-doc-examples
Multi-Arch: foreign
Description: 3D drawing with MetaPost output -- documentation
 GNU 3DLDF implements an interpreter for a METAFONT-like language for
 three-dimensional drawing with MetaPost output.
 .
 GNU 3DLDF is mainly intended to provide a convenient way of creating
 3D graphics for inclusion in TeX documents; it can also be used for
 creating animations, which can contain text typeset using TeX.
 .
 This package contains the documentation for 3DLDF.
